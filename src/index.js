import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import calculatorApp from './reducers'
import promiseMiddleware from 'redux-promise';

const middleWare = applyMiddleware(promiseMiddleware)
export let store = createStore(calculatorApp, middleWare)

render(
    <Provider store={store}>
    <App/>
    </Provider>,
    document.getElementById('root')
)
