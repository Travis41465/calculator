import { combineReducers } from 'redux'
import answer from './answer'
import numbers from './numbers'
import operator from './operator'

const calculatorApp = combineReducers({
    answer,
    numbers,
    operator
})

export default calculatorApp