const numbers = (state = [], action) => {
    switch(action.type) {
        case "ADD_NUMBER" : 
        return [
            ...state,
            action.number
        ]
        default :
        return state
    }
}

const getFirstNumberEntered = state => {
    return state.numbers[0]
}

const getSecondNumberEntered = state => {
    return state.numbers[1]
}

export default numbers
