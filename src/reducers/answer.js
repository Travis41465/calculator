const answer = (state = 0, action) => {
    switch(action.type) {
        case 'SOLVE_PROBLEM' : 
            return action.answer
        default :
            return ""
    }
}

export default answer