export const addNumber = number => {
    return {
        type : "ADD_NUMBER",
        number
    }
}

export const addOperator = operator => {
    return {
        type : "ADD_OPERATOR",
        operator
    }
}

export const solveProblem = ({num1, operator, num2}) => {
    let problemAnswer
    const firstNum = num1
    const secondNum = num2

    switch(operator) {
        case '+' : problemAnswer = firstNum + secondNum
        break
        case '-' : problemAnswer = firstNum - secondNum
        break
        case '*' : problemAnswer = firstNum * secondNum
        break
        case '/' : problemAnswer = firstNum / secondNum
        break
        default : problemAnswer = 0
    }

    return {
        type : "SOLVE_PROBLEM",
        answer : problemAnswer
    }
}