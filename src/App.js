import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import AddNumber from './containers/AddNumber'
import ViewScreen from './containers/ViewScreen'
import AddOperator from './containers/AddOperator'
import GetAnswer from './containers/GetAnswer'

class App extends Component {
  render() {
    return (
      <div>
      <ViewScreen/>
      <div>
        <AddNumber/>
        <AddOperator/>
        <GetAnswer/>
      </div>
      </div>
    );
  }
}

export default App;
