import React from 'react'


const OperatorButtons = ({onClick}) => {
    return(
        <div>
        <button onClick={e => {
            e.preventDefault()
            onClick('+')
        }}>
            +
        </button>
        <button onClick={() => onClick('-')}>
            -
        </button>
        <button onClick={() => onClick('*')}>
            *
        </button>
        <button onClick={() => onClick('/')}>
            /
        </button>
        </div>
    )
}


export default OperatorButtons