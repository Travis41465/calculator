import React from 'react'

const EqualsButton = ({onClick}) => {
    return(
        <button onClick={() => onClick()}>
            =
        </button>
    )
}

export default EqualsButton