import React from 'react'

const Screen = ({firstNum, operator, secondNum, answer}) => {
    return(
        <h2>{firstNum} {operator} {secondNum} {answer} </h2>
    )
}

export default Screen