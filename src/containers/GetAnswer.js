import { connect } from 'react-redux'
import { solveProblem } from '../actions'
import EqualsButton from '../components/EqualsButton'
import { store } from '../index'


const mapDispatchToProps = (dispatch,state) => {
    return {
        onClick:() => {
            const num1 = store.getState().numbers[0]
            const operator = store.getState().numbers[1]
            const num2 = store.getState().numbers[2]
            dispatch(solveProblem({num1, operator, num2}))
        }
    }
}

const GetAnswer = connect(undefined, mapDispatchToProps)(EqualsButton)

export default GetAnswer