import { connect } from 'react-redux'
import Screen from '../components/Screen'

const mapStateToProps = state => {
    return {
        firstNum : state.numbers[0],
        operator : state.numbers[1],
        secondNum : state.numbers[2],
        answer : state.answer
    }
}

const ViewScreen = connect(mapStateToProps, undefined)(Screen)

export default ViewScreen