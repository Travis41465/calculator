import { connect } from 'react-redux'
import OperatorButtons from '../components/OperatorButtons'
import { addNumber } from '../actions'

const mapDispatchToProps = dispatch => {
    return {
        onClick : (operator) => {
            dispatch(addNumber(operator))
        }
    }
}

const AddOperator = connect(undefined, mapDispatchToProps)(OperatorButtons)

export default AddOperator