import { connect } from 'react-redux'
import { addNumber } from '../actions'
import Buttons from '../components/Buttons'

const mapDispatchToProps = (dispatch,state) => {
    return {
        onClick : (number) => {
            dispatch(addNumber(number))
        }
    }
}

const AddNumber = connect(undefined, mapDispatchToProps)(Buttons)

export default AddNumber